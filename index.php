<?php
include __DIR__ . '/vendor/autoload.php';

Dupont\MVC\SRC\Routing::routing::
$routing = new \Dupont\MVC\Routing();
$Routing->init();
echo $routing->getEntity();
echo $routing->getAction();

    $pos = strrpos($_SERVER['SCRIPT_NAME'], '/');
    $path = substr($_SERVER['SCRIPT_NAME'], 0, $pos + 1);


    $uc = strtolower(str_replace($path, '', $_SERVER['REDIRECT_URL']));

    $ucArray = explode('/' , $uc);


    $view = __DIR__ . '/vendor/dupont/competition/src/View/Admin/Overview.php';
    switch ($ucArray[0])
    {
        case 'player' : {
            switch ($ucArray[1]) {
                case 'create' :
                    echo 'You will create a player';
                    break;
                 case 'readingone' :
                     $player = new \Dupont\Competition\Model\Player();
                     $player->setLastName('Dupont');
                     $player->setFirstName('Kim');
                     $player->setAddress('Lisbon');
                     $player->setShirtNumber('13');
                     $view = __DIR__ . '/vendor/dupont/competition/src/View/Player/ReadingOne.php';
                     break;
                case 'updatingone' :
                    // in het echt lezen we de gegevens uit de database in
                    // connectie met mysql
                     $player = new \Dupont\Competition\Model\Player();
                     $player->setLastName('Dupont');
                     $player->setFirstName('Kim');
                     $player->setAddress('Lisbon');
                     $player->setShirtNumber('1');
                     $view = __DIR__ . '/vendor/dupont/competition/src/View/Player/UpdatingOne.php';
                    break;
                 case 'creatingone' :
                    $view = __DIR__ . '/vendor/dupont/competition/src/View/Player/CreatingOne.php';
                    break;
                 case 'editing' :
                    $view = __DIR__ . '/vendor/modernways/competition/src/View/Player/Editing.php';
                    break;
                case 'delete' :
                    echo 'je gaat een speler deleten';
                    break;
          }
        }
        case 'liga' : {
            switch ($ucArray[1]) {
                case 'create' :
                    echo 'je gaat een speler aanmaken';
                    break;
                 case 'read' :
                    echo 'je gaat een speler inlezen';
                    break;
                case 'update' :
                    echo 'je gaat een speler updaten';
                    break;
                 case 'delete' :
                    echo 'je gaat een speler deleten';
                    break;
          }
        }
    }



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Main page</title>
</head>
<body>
<h1>This is the page that is being called when the visitor arrives at our competition page.</h1>
</body>
</html>

